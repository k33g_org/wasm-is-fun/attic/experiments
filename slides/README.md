# slides

```bash
marp slides.md --html true
```


## Refs

- https://twitter.com/rverchere/status/1435889620475125763
- https://medium.com/linkbynet/making-slides-from-anywhere-for-anyone-using-marp-gitlab-pages-and-gitpod-35001daf1c93

#!/bin/bash
cd hello; wasm-pack build
cd ..
cd hey; wasm-pack build
cd ..

cp ./hello/pkg/hello_lib_bg.wasm ./faas/rust.hello.wasm
cp ./hey/pkg/hey_lib_bg.wasm ./faas/rust.hey.wasm


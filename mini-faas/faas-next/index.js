const pkg = require('./rust.wasm.loader.js')
const { handle, initializeWasm } = pkg

initializeWasm('./rust.yo.wasm').then(() => {
  console.log(handle(
    JSON.stringify({
      firstname:"Bob",
      lastname: "Morane"
    }),
    JSON.stringify({
      demo_token: "this is a token"
    })
  )) 

}).catch(error => {
  console.error("😡", error)
})






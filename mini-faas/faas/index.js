const pkg = require('./rust.wasm.loader.js')
const { handle, initializeWasm } = pkg

initializeWasm('./rust.hello.wasm').then(() => {
  console.log(handle("👋 Bob")) 
  console.log(handle("😃 Jane")) 
  console.log(handle("🖐️ John")) 

  initializeWasm('./rust.hey.wasm').then(() => {
    console.log(handle("👋 Bob")) 
    console.log(handle("😃 Jane")) 
    console.log(handle("🖐️ John")) 
  }).catch(error => {
    console.error("😡", error)
  })
  
}).catch(error => {
  console.error("😡", error)
})






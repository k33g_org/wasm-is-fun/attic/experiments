

## Hello Web

```bash
cargo new --lib hello
```

add this to `./hello/Cargo.toml`:

```toml
[lib]
name = "hello_lib"
path = "src/lib.rs"
crate-type =["cdylib"]

[dependencies]
wasm-bindgen = "0.2.50"
```

replace content of `lib.rs` by:

```rust
use wasm_bindgen::prelude::*;
#[wasm_bindgen]
pub fn hello(s: String) -> String {
  let r = String::from("hello ");
  return r + &s;
}
```

```bash
cd hello
wasm-pack build --target web

cp ./hello/pkg/hello_lib_bg.wasm ./hello-web/
cp ./hello/pkg/hello_lib.js ./hello-web/
```

## Hello Node

About the code, don't change anything (follow the same steps as the former section)

```bash
cd hello
wasm-pack build --target nodejs

cp ./hello/pkg/hello_lib_bg.wasm ./hello-node/
cp ./hello/pkg/hello_lib.js ./hello-node/
```
